# There are appliances for creative people in this repository.

# etherwave-clone
Clone of old theremin from the Moog lab with SMD components.
Fully planar clone with wide range of power supply (3-14V).
![f](etherwave\-clone/pcb/sch_small.png)\
![ff](etherwave\-clone/pcb/pcb3d.png)\